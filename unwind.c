#include "unwind.h"

#include <stdio.h>

#include <libunwind-ptrace.h>
#ifdef __x86_64__
#include <libunwind-x86_64.h>
#define REG_WIDTH "16"
#define REG_UNK "????????????????"
#endif

#include "hexdump.h"

static void do_one_register(unw_cursor_t * c, const char * name, unw_regnum_t reg) {
	unw_word_t val;
	if (unw_get_reg(c, reg, &val))
		printf("%s=%s ", name, REG_UNK);
	else
		printf("%s=%0" REG_WIDTH "lx ", name, (long)val);
}

static void do_registers(unw_cursor_t * c) {
#ifdef __x86_64__
	do_one_register(c, "rax", UNW_X86_64_RAX);
	do_one_register(c, "rbx", UNW_X86_64_RBX);
	do_one_register(c, "rcx", UNW_X86_64_RCX);
	do_one_register(c, "rdx", UNW_X86_64_RDX);
	puts("");
	do_one_register(c, "rsi", UNW_X86_64_RSI);
	do_one_register(c, "rdi", UNW_X86_64_RDI);
	do_one_register(c, "rbp", UNW_X86_64_RBP);
	do_one_register(c, "rsp", UNW_X86_64_RSP);
	puts("");
	do_one_register(c, " r8", UNW_X86_64_R8);
	do_one_register(c, " r9", UNW_X86_64_R9);
	do_one_register(c, "r10", UNW_X86_64_R10);
	do_one_register(c, "r11", UNW_X86_64_R11);
	puts("");
	do_one_register(c, "r12", UNW_X86_64_R12);
	do_one_register(c, "r13", UNW_X86_64_R13);
	do_one_register(c, "r14", UNW_X86_64_R14);
	do_one_register(c, "r15", UNW_X86_64_R15);
	puts("");
#endif
}

void do_unwind(pid_t pid) {
	unw_addr_space_t as = unw_create_addr_space(&_UPT_accessors, 0);
	if (!as) {
		fprintf(
			stderr,
			"Backtrace failed: unw_create_addr_space() returned NULL\n"
		);
		goto cleanup;
	}
	void * ctx = _UPT_create(pid);
	if (!ctx) {
		fprintf(
			stderr,
			"Backtrace failed: _UPT_create() returned NULL\n"
		);
		goto cleanup;
	}

	unw_cursor_t cursor;
	int err = unw_init_remote(&cursor, as, ctx);
	if (err) {
		fprintf(
			stderr,
			"Backtrace failed: unw_init_remote() returned error %d\n",
			err
		);
		goto cleanup;
	}

	do_registers(&cursor);

	unw_word_t ip, prev_sp = 0, sp, off;
	do {
		unw_get_reg(&cursor, UNW_REG_IP, &ip);
		unw_get_reg(&cursor, UNW_REG_SP, &sp);
		if (prev_sp) hexdump(pid, prev_sp, sp);
		static char name[0x400];
		printf(
			"ip = %lx, sp = %lx",
			(long)ip, (long)sp
		);
		int err = unw_get_proc_name(&cursor, name, sizeof name, &off);
		if (err == 0 || err == UNW_ENOMEM) printf(
			" [%s%+ld]", name, (long)off
		);
		prev_sp = sp;
		puts("");
	} while (unw_step(&cursor) > 0);

cleanup:
	if (ctx) _UPT_destroy(ctx);
	if (as) unw_destroy_addr_space(as);
}
