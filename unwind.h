#include <sys/types.h>

// Given the PID of a stopped tracee, obtain a traceback and print it on
// standard output.
void do_unwind(pid_t);
