#include "hexdump.h"

#include <errno.h>
#include <stdbool.h>
#include <sys/ptrace.h>
#include <stdio.h>

#ifdef HEXDUMP_LIMIT
// Load a `long` from the specified address, which probably must be
// aligned.
static bool peek(pid_t pid, uintptr_t addr, unsigned long * val) {
	// It would have been easier to use the underlying API where the
	// data is written via a pointer, not as a return value.
	errno = 0;
	long ret = ptrace(PTRACE_PEEKDATA, pid, (void*)addr, 0);
	if (ret == -1 && errno) return false;

	*val = (unsigned long)ret;
	return true;
}

// Load a byte from the specified address, which doesn't have to be
// aligned. Cache previous memory accesses.
static bool maybe_peek(pid_t pid, uintptr_t addr, uint8_t * data) {
	static union {
		unsigned long l;
		uint8_t b[sizeof(long)];
	} cache;
	static uintptr_t cache_addr;
	static bool cache_success;

	uintptr_t peekaddr = addr & ~(sizeof(cache.l) - 1);
	if (peekaddr != cache_addr || !cache_success) {
		cache_addr = peekaddr;
		cache_success = peek(pid, peekaddr, &cache.l);
	}
	if (cache_success) {
		*data = cache.b[addr - cache_addr];
		return true;
	}
	return false;
}
#endif

void hexdump(pid_t pid, unw_word_t from, unw_word_t to) {
#ifndef HEXDUMP_LIMIT
	(void)pid; (void)from; (void)to;
#else
	// Try to accomodate either stack growth direction
	if (from > to) {
		unw_word_t temp = from;
		from = to;
		to = temp;
	}
	// Avoid dumping the consequences of stack overflow
	if ((to - from) > (unw_word_t)(HEXDUMP_LIMIT))
		to = from + (HEXDUMP_LIMIT);

	// Reset the ptrace peek cache
	maybe_peek(pid, 0, (uint8_t[1]){});

	// print values in rows of 16 octets
	for (uintptr_t row = from & ~0xf; row < to; row += 16) {
		printf("%lx ", row);
		// first the hex codes
		for (int i = 0; i < 16; ++i) {
			if (row + i < from || row + i >= to) {
				printf("   ");
				continue;
			}
			uint8_t octet;
			if (maybe_peek(pid, row + i, &octet))
				printf("%02x ", octet);
			else
				printf("?? ");
		}
		// now the ASCII codes
		for (int i = 0; i < 16; ++i) {
			uint8_t octet;
			if (
				row + i >= from && row + i < to
				&& maybe_peek(pid, row + i, &octet)
				&& octet >= 0x20 && octet < 0x7f
			)
				printf("%c", octet);
			else
				printf(" ");
		}
		puts("");
	}
#endif
}
