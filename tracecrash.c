#include <errno.h>
#include <signal.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ptrace.h>
#include <sys/wait.h>
#include <unistd.h>

#ifndef __linux__
#error Only the Linux ptrace() interface is supported.
#endif

#include "unwind.h"

// Fork off a new tracee with a given command line.
// The tracee will stop before exec() to set up the process.
static pid_t run_child(char ** argv) {
	pid_t pid = fork();
	if (pid < 0) {
		perror("fork()");
		return -1;
	} else if (pid == 0) {
		if (ptrace(PTRACE_TRACEME, 0, 0, 0)) {
			fprintf(stderr, "ptrace(PTRACE_TRACEME): %s\n", strerror(errno));
			return -1;
		}
		raise(SIGSTOP); // to let the tracer set everything up
		execvp(*argv, argv);
		// still there?
		fprintf(stderr, "execvp(\"%s\", ...): %s\n", *argv, strerror(errno));
		exit(-1);
	}
	return pid;
}

static bool setup_tracee(pid_t pid) {
	bool ret = true;
	ret = (ptrace(
		PTRACE_SETOPTIONS, pid, 0,
		(void*)(
			// must trace all threads and children
			PTRACE_O_TRACECLONE | PTRACE_O_TRACEFORK | PTRACE_O_TRACEVFORK
			// don't actually care about exec(), but extra SIGTRAP is annoying
			| PTRACE_O_TRACEEXEC
		)
	) == 0) && ret;
	ret = (ptrace(PTRACE_CONT, pid, 0, 0) == 0) && ret;
	return ret;
}

typedef struct {
	uintptr_t arg;
	pid_t pid;
	enum pevent_type {
		PEVENT_NEWPROC, // set up new children and resume, arg = new PID
		PEVENT_CRASH, // backtrace and resume, arg = signal
		PEVENT_OTHER, // just resume, arg = signal or 0
		PEVENT_EXIT, // tracee terminated, arg = pid or 0 if no tracees left
	} event;
} pevent;

static pevent get_one_event(void) {
	int status;
	pid_t pid = waitpid(-1, &status, __WALL);

	// All tracees lost in time, like tears in rain... Time to die.
	if (pid == -1) return (pevent){ .pid = 0, .event = PEVENT_EXIT };

	// Got a ptrace() event?
	if (WIFSTOPPED(status)) {
		if (
			(status>>8 == (SIGTRAP | (PTRACE_EVENT_CLONE<<8)))
			|| (status>>8 == (SIGTRAP | (PTRACE_EVENT_FORK<<8)))
			|| (status>>8 == (SIGTRAP | (PTRACE_EVENT_VFORK<<8)))
		) {
			// New arrival!
			unsigned long newpid;
			ptrace(PTRACE_GETEVENTMSG, pid, 0, &newpid);
			return (pevent){ .pid = pid, .event = PEVENT_NEWPROC, .arg = newpid };
		}
		// We've set this to avoid the SIGTRAP on exec() that would have
		// happened otherwise.
		if ((status>>8 == (SIGTRAP | (PTRACE_EVENT_EXEC<<8))))
			return (pevent){ .pid = pid, .event = PEVENT_OTHER };

		// Was it a signal?
		siginfo_t sig;
		if (!ptrace(PTRACE_GETSIGINFO, pid, 0, &sig)) {
			switch (sig.si_signo) {
			case SIGBUS:
			case SIGILL:
			case SIGSEGV:
			case SIGTRAP:
				// Yes, and a crashing one!
				return (pevent){
					.pid = pid, .event = PEVENT_CRASH, .arg = sig.si_signo
				};
			default:
				// Yes, but we're not interested
				return (pevent){
					.pid = pid, .event = PEVENT_OTHER, .arg = sig.si_signo
				};
			}
		}

		// What? Resume the process anyway.
		return (pevent){ .pid = pid, .event = PEVENT_OTHER };
	}

	// Something must have died
	return (pevent){
		.pid = pid, .event = PEVENT_EXIT,
		.arg =
			WIFEXITED(status) ? WEXITSTATUS(status)
			: WIFSIGNALED(status) ? (0x80 | WTERMSIG(status))
			: (status >> 8)
	};
}

int main(int argc, char ** argv) {
	if (argc <= 1) {
		fprintf(stderr,
			"Usage: %s <program> [further arguments to program]\n",
			*argv ? *argv : "tracecrash"
		);
		return 255;
	}

	pid_t child = run_child(argv+1);
	if (child == -1) return 254;

	waitpid(child, NULL, 0); // observe the SIGSTOP
	setup_tracee(child);

	int exit_status = 253;
	for (;;) {
		pevent event = get_one_event();
		switch (event.event) {
		case PEVENT_NEWPROC:
			// set up and resume child
			setup_tracee(event.arg);
			// resume parent
			ptrace(PTRACE_CONT, event.pid, 0, 0);
			break;
		case PEVENT_CRASH:
			puts(
				"-----------------------------------"
				"8<"
				"-----------------------------------"
			);
			printf(
				"PID %d received crashing signal %d\n",
				event.pid, (int)event.arg
			);
			do_unwind(event.pid);
			puts(
				"-----------------------------------"
				"8<"
				"-----------------------------------"
			);
			// FALLTHROUGH
		case PEVENT_OTHER:
			// resume, delivering the signal
			ptrace(PTRACE_CONT, event.pid, 0, (void*)event.arg);
			break;
		case PEVENT_EXIT:
			if (event.pid == child) exit_status = event.arg;
			if (event.pid == 0) goto done;
		}
	}
done:
	return exit_status;
}
